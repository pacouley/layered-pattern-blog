package org.gmslabs.api.service.impl;

import org.gmslabs.api.model.Personne;
import org.gmslabs.api.repository.PersonneRepository;
import org.gmslabs.api.service.PersonneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PersonneServiceImpl implements PersonneService {
    @Autowired
    private PersonneRepository personneRepository;

    @Override
    public Personne obtenirPersonneByEmail(String email){
        return personneRepository.findByEmail(email);
    }
}
