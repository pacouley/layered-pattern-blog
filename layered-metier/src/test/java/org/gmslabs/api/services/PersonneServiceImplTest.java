package org.gmslabs.api.services;

import org.gmslabs.api.model.Personne;
import org.gmslabs.api.repository.PersonneRepository;
import org.gmslabs.api.service.PersonneService;
import org.gmslabs.api.service.impl.PersonneServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class PersonneServiceImplTest {

    @TestConfiguration
    static class PersonneServiceImplTestContextConfiguration {
        @Bean
        public PersonneService personneService() {
            return new PersonneServiceImpl();
        }
    }

    @Autowired
    private PersonneService personneService;

    @MockBean
    private PersonneRepository personneRepository;

    @Before
    public void setUp() {
        Personne personne = new Personne();
        personne.setEmail("email");
        Mockito.when(personneRepository.findByEmail(personne.getEmail())).thenReturn(personne);
    }

    @Test
    public void testFindPersonneByEmail() {
        String email = "email";
        Personne found = personneService.obtenirPersonneByEmail(email);
        assertThat(found.getEmail()).isEqualTo(email);
    }
}
