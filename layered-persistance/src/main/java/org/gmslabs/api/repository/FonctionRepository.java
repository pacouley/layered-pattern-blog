package org.gmslabs.api.repository;

import org.gmslabs.api.model.Fonction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FonctionRepository extends JpaRepository<Fonction, Long> {
    Fonction findByCode(String code);
}

