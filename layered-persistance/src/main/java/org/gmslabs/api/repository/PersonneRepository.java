package org.gmslabs.api.repository;

import org.gmslabs.api.model.Personne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonneRepository extends JpaRepository<Personne, Long> {
    Personne findByEmail(String email);
}

