package org.gmslabs.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Objet asssocié au informations de la personne
 *
 * @author pacouley
 */
@Entity
@Table(name = "fonction")
@Data
@NoArgsConstructor
public class Fonction extends BaseEntity {
    private String code;
    private String libelle;
}
