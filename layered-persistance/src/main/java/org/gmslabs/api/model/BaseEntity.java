package org.gmslabs.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

/**
 * Objet asssocié
 *
 * @author pacouley
 */
@MappedSuperclass
@Data
@NoArgsConstructor
public class BaseEntity implements Serializable {

    /**
     * Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * La date de la création de l'enregistrement dans la base.
     */
    private Date dateCreaEnr;

    /**
     * La date de modification dans la base.
     */
    private Date dateModifEnr;

    /**
     * Code de la derniere personne à avoir modifié l'objet.
     */
    private String codeUserToUpdate;

    /**
     * Code de la personne qui a cree l'objet.
     */
    private String codeUserToCreate;
}
