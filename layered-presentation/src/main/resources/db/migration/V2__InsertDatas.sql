insert into personne(id,code_user_to_create,email,nom,prenom)
        values(1,'email@test.com','p.dubois@mail.com','DUBOIS','Paul');
insert into personne(id,code_user_to_create,email,nom,prenom)
        values(2,'email@test.com','j.leti@mail.com','LETI','Jeanne');
insert into personne(id,code_user_to_create,email,nom,prenom)
        values(3,'email@test.com','j.zago@mail.com','ZAGO','John');
insert into personne(id,code_user_to_create,email,nom,prenom)
        values(4,'email@test.com','m.wetch@mail.com','WETCH','Marc');

insert into fonction(id,code_user_to_create,code,libelle)
        values(1,'email@test.com','MEDE','Medecin');
insert into fonction(id,code_user_to_create,code,libelle)
        values(2,'email@test.com','ASSO','Assistance sociale');

insert into personne_fonction(personnefk,fonctionfk)
        values((select id from personne where email='p.dubois@mail.com'),(select id from fonction where code='MEDE'));
insert into personne_fonction(personnefk,fonctionfk)
        values((select id from personne where email='j.leti@mail.com'),(select id from fonction where code='MEDE'));
insert into personne_fonction(personnefk,fonctionfk)
        values((select id from personne where email='j.zago@mail.com'),(select id from fonction where code='ASSO'));
insert into personne_fonction(personnefk,fonctionfk)
        values((select id from personne where email='m.wetch@mail.com'),(select id from fonction where code='ASSO'));